package net.bor.discordguard;

public class Main {
    public static void main(String[] args) {
        try {
            new DiscordGuard(args).init();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            Runtime.getRuntime().halt(1);

        }
    }
}
