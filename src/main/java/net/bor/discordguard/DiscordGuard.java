package net.bor.discordguard;

import lombok.Generated;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.bor.discordguard.config.Config;
import net.bor.discordguard.config.IConfig;
import net.bor.discordguard.db.DataBases;
import net.bor.discordguard.utilities.migrations.Migrations;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;

import java.io.File;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;

// @author ArtBorax
public class DiscordGuard {
    @Getter
    public final ArrayList<String> args;

    @Getter
    public static DiscordGuard instance;

    @Getter
    private final org.apache.logging.log4j.Logger logger = LogManager.getLogger(DiscordGuard.class);

    @Getter
    @Generated
    private DataBases database;

    DiscordGuard(@NonNull String[] args) {
        this.args = new ArrayList<>(Arrays.asList(args));
    }

    public void init() throws Throwable {
        this.initLogger();

        logger.info("******************************");
        logger.info("Discord Guard Service");
        logger.info("http://mcborax.ru");
        logger.info("******************************");

        if (this.args.contains("--debug")) {
            logger.debug("DEBUG ON");
        }

        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));

        instance = this;

        IConfig config = new Config();
        database = new DataBases(config);

        if (!args.contains("--no-migrations"))
            this.initMigrations();
    }

    private void initLogger() throws URISyntaxException {
        String name = "log4j2.xml";
        if (this.args.contains("--debug")) {
            name = "log4j2-debug.xml";
        }

        LoggerContext.getContext(false).setConfigLocation(getClass().getClassLoader().getResource(name).toURI());
    }

    private void initMigrations() {
        Migrations migrationModule = new Migrations(database.getConnectDBDiscordGuard());
        migrationModule.init();
        try {
            migrationModule.migrateUp();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void shutdown() {
        // TODO: Сделать отключение от всех сторонних сервисов.
        logger.info("Shutdown service. Goodbye!");
    }
}
