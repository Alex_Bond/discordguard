package net.bor.discordguard.dao;

// @author ArtBorax
import net.bor.discordguard.dao.map.Server;

public interface IDataBase {

    public void shutdown();

    public Server getServer(int id);
}
