package net.bor.discordguard.dao.map;

// @author ArtBorax
import java.util.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "reports")
public class Report {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "report_id")
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "creator_player_id")
    private Player creator;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "suspected_player_id")
    private Player suspected;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "server_id")
    private Server server;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "report_id")
    private Set<ReportResult> results;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "report_id")
    private Set<ReportMsg> msgs;

    @Column(name = "type")
    private Integer type;

    @Column(name = "karma")
    private Integer karma;

    @Column(name = "penalty")
    private Integer penalty;

    @Column(name = "time")
    private Date time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Player getCreator() {
        return creator;
    }

    public void setCreator(Player creator) {
        this.creator = creator;
    }

    public Player getSuspected() {
        return suspected;
    }

    public void setSuspected(Player suspected) {
        this.suspected = suspected;
    }

    public Server getServer() {
        return server;
    }

    public void setServer(Server server) {
        this.server = server;
    }

    public Set<ReportResult> getResults() {
        return results;
    }

    public void setResults(Set<ReportResult> results) {
        this.results = results;
    }

    public Set<ReportMsg> getMsgs() {
        return msgs;
    }

    public void setMsgs(Set<ReportMsg> msgs) {
        this.msgs = msgs;
    }

    public Integer getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Integer getKarma() {
        return karma;
    }

    public void setKarma(int karma) {
        this.karma = karma;
    }

    public Integer getPenalty() {
        return penalty;
    }

    public void setPenalty(int penalty) {
        this.penalty = penalty;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

}
