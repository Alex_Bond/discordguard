package net.bor.discordguard.dao.map;

// @author ArtBorax
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "banv3")
public class Ban {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "username")
    private String username;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    private Ban parent;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent")
    private Set<Ban> children;

    @Column(name = "date")
    private Integer date;

    public String getUsername() {
        return username;
    }

    public Ban getParent() {
        return parent;
    }

    public Set<Ban> getChildren() {
        return children;
    }

    public Integer getDate() {
        return date;
    }
    
    
}
