package net.bor.discordguard.dao.map;

// @author ArtBorax
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "players")
public class Player {

    @Id
    @Column(name = "player_id")
    private int playerId;

    @Column(name = "discord_user_id")
    private Long discordUserID;

    @Column(name = "username", length = 30)
    private String username;

    @Column(name = "uuid", length = 60)
    private String uuid;

    @Column(name = "karma")
    private Double karma;

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public Long getDiscordUserID() {
        return discordUserID;
    }

    public void setDiscordUserID(long discordUserID) {
        this.discordUserID = discordUserID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Double getKarma() {
        return karma;
    }

    public void setKarma(Double karma) {
        this.karma = karma;
    }

}
