package net.bor.discordguard.dao.map;

// @author ArtBorax
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "players_info")
@IdClass(PlayerInfoPK.class)
public class PlayerInfo {

    public PlayerInfo(PlayerInfoPK key) {
        player = key.getPlayer();
        server = key.getServer();
    }

    @Id
    @AttributeOverrides({
        @AttributeOverride(name = "player", column = @Column(name = "player_id")),
        @AttributeOverride(name = "server", column = @Column(name = "server_id"))})

    private Player player;
    private Server server;

    @Column(name = "time_join")
    private Date timeJoin;

    @Column(name = "time_quit")
    private Date timeQuit;

    @Column(name = "time_online")
    private Date timeOnline;

    @Column(name = "join_count")
    private Integer joinCount;

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Server getServer() {
        return server;
    }

    public void setServer(Server server) {
        this.server = server;
    }

    public Date getTimeJoin() {
        return timeJoin;
    }

    public void setTimeJoin(Date timeJoin) {
        this.timeJoin = timeJoin;
    }

    public Date getTimeQuit() {
        return timeQuit;
    }

    public void setTimeQuit(Date timeQuit) {
        this.timeQuit = timeQuit;
    }

    public Date getTimeOnline() {
        return timeOnline;
    }

    public void setTimeOnline(Date timeOnline) {
        this.timeOnline = timeOnline;
    }

    public Integer getJoinCount() {
        return joinCount;
    }

    public void setJoinCount(int joinCount) {
        this.joinCount = joinCount;
    }

}
