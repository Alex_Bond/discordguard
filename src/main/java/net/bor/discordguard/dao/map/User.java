package net.bor.discordguard.dao.map;

// @author ArtBorax
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ygid3_users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String pass;

    @Column(name = "block")
    private Integer block;

    @Column(name = "registerDate")
    private Date registerDate;

    @Column(name = "lastvisitDate")
    private Date lastVisitSiteDate;

    @Column(name = "mc_date")
    private Integer lastJoinServerTime;

    @Column(name = "uuid")
    private String uuid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Integer getBlock() {
        return block;
    }

    public void setBlock(int block) {
        this.block = block;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public Date getLastVisitSiteDate() {
        return lastVisitSiteDate;
    }

    public Integer getLastJoinServerTime() {
        return lastJoinServerTime;
    }

    public String getUuid() {
        return uuid;
    }

}
