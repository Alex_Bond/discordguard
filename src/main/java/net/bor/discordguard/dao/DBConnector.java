package net.bor.discordguard.dao;

import java.util.HashMap;
import java.util.Map;

import net.bor.discordguard.DiscordGuard;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;

// @author ArtBorax
public class DBConnector {

    private StandardServiceRegistry registry;
    private SessionFactory sessionFactory;
    private Session session;
    private final String host;
    private final String dbName;
    private final String username;
    private final String pass;
    private final Class[] clazz;

    public DBConnector(String host, String dbName, String username, String pass, Class... clazz) {
        this.host = host;
        this.dbName = dbName;
        this.username = username;
        this.pass = pass;
        this.clazz = clazz;
    }

    public SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                DiscordGuard.getInstance().getLogger().info("Connecting to database - " + dbName);

                Map<String, String> settings = new HashMap<>();
                settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
                settings.put(Environment.URL, "jdbc:mysql://" + host + "/" + dbName + "?characterEncoding=UTF-8&serverTimezone=UTC");
                settings.put(Environment.USER, username);
                settings.put(Environment.PASS, pass);
                settings.put(Environment.DIALECT, org.hibernate.dialect.MySQL5Dialect.class.getCanonicalName());
                //validate - проверяет схему
                //update - обновляет и создает новые таблицы
                //crate - !удаляются все таблицы и создаются снова!
                //crate-drop - !!!уничтожает базу по закрытию сейщенфэктори!!!
                settings.put(Environment.HBM2DDL_AUTO, "update");
                settings.put(Environment.SHOW_SQL, "true"); //debug

                registry = new StandardServiceRegistryBuilder().applySettings(settings).build();
                MetadataSources meta = new MetadataSources(registry);
                for (Class claz : clazz) {
                    meta.addAnnotatedClass(claz);
                }
                sessionFactory = meta.buildMetadata().buildSessionFactory();
                DiscordGuard.getInstance().getLogger().info("Connection is ready!");
            } catch (Exception e) {
                e.printStackTrace();
                if (registry != null) {
                    StandardServiceRegistryBuilder.destroy(registry);
                    sessionFactory = null;
                }
            }
        }
        return sessionFactory;
    }

    public Session getSession() {
        if (sessionFactory == null) {
            getSessionFactory();
        }
        if (session == null && sessionFactory != null) {
            session = sessionFactory.openSession();
        }
        return session;
    }

    public void shutdown() {
        if (registry != null) {
            StandardServiceRegistryBuilder.destroy(registry);
            sessionFactory = null;
            DiscordGuard.getInstance().getLogger().info("Disconnecting from database - " + dbName);
        }
    }
}
