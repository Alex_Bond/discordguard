package net.bor.discordguard.dao;

// @author ArtBorax

import net.bor.discordguard.config.IConfig;
import net.bor.discordguard.dao.map.*;

public class DataBase implements IDataBase {

    private final IConfig config;
    private final DBConnector dbDiscordGuard;
    private final DBConnector dbLauncher;
    private final DBConnector dbSite;

    public DataBase(IConfig config) {
        this.config = config;
        dbDiscordGuard = new DBConnector(config.getDBDiscordGuardHost(), config.getDBDiscordGuardName(), config.getDBDiscordGuardUsername(), config.getDBDiscordGuardPass(),
                Player.class, Chat.class, Server.class, QueueMsg.class, Report.class, ReportMsg.class, ReportResult.class, PlayerInfo.class, PlayerInfoPK.class
        );
        dbSite = new DBConnector(config.getDBUsersHost(), config.getDBUsersName(), config.getDBUsersUsername(), config.getDBUsersPass(),
                User.class
        );
        dbLauncher = new DBConnector(config.getDBLauncherHost(), config.getDBLauncherName(), config.getDBLauncherUsername(), config.getDBLauncherPass(),
                Ban.class
        );
    }

    @Override
    public void shutdown() {
        dbLauncher.shutdown();
        dbSite.shutdown();
        dbDiscordGuard.shutdown();
    }

    @Override
    public Server getServer(int id) {
        return dbDiscordGuard.getSession().get(Server.class, id);
    }

}
