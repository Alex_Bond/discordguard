package net.bor.discordguard.db;

// @author ArtBorax

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.bor.discordguard.DiscordGuard;
import net.bor.discordguard.config.IConfig;

public class DataBases {
    private final IConfig config;
    private Connection connect = null;
    private Connection connectUsers = null;
    private Connection connectLauncher = null;

    public DataBases(IConfig config) {
        this.config = config;
        getConnectDBDiscordGuard();
//        getConnectDBUsers();
//        getConnectDBLauncher();
    }

    public Connection getConnectDBDiscordGuard() {
        if (connect == null) {
            connect = connectDB(connect, config.getDBDiscordGuardUsername(), config.getDBDiscordGuardPass(), config.getDBDiscordGuardHost(), config.getDBDiscordGuardName());
        }
        try {
            if (connect.isClosed()) {
                connect = connectDB(connect, config.getDBDiscordGuardUsername(), config.getDBDiscordGuardPass(), config.getDBDiscordGuardHost(), config.getDBDiscordGuardName());
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            connect = connectDB(connect, config.getDBDiscordGuardUsername(), config.getDBDiscordGuardPass(), config.getDBDiscordGuardHost(), config.getDBDiscordGuardName());
        }
        return connect;
    }

    public Connection getConnectDBUsers() {
        if (connectUsers == null) {
            connectUsers = connectDB(connectUsers, config.getDBUsersUsername(), config.getDBUsersPass(), config.getDBUsersHost(), config.getDBUsersName());
        }
        try {
            if (connectUsers.isClosed()) {
                connectUsers = connectDB(connectUsers, config.getDBUsersUsername(), config.getDBUsersPass(), config.getDBUsersHost(), config.getDBUsersName());
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            connectUsers = connectDB(connectUsers, config.getDBUsersUsername(), config.getDBUsersPass(), config.getDBUsersHost(), config.getDBUsersName());
        }
        return connectUsers;
    }

    public Connection getConnectDBLauncher() {
        if (connectLauncher == null) {
            connectLauncher = connectDB(connectLauncher, config.getDBLauncherUsername(), config.getDBLauncherPass(), config.getDBLauncherHost(), config.getDBLauncherName());
        }
        try {
            if (connectLauncher.isClosed()) {
                connectLauncher = connectDB(connectLauncher, config.getDBLauncherUsername(), config.getDBLauncherPass(), config.getDBLauncherHost(), config.getDBLauncherName());
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            connectLauncher = connectDB(connectLauncher, config.getDBLauncherUsername(), config.getDBLauncherPass(), config.getDBLauncherHost(), config.getDBLauncherName());
        }
        return connectLauncher;
    }

    private Connection connectDB(Connection conn, String user, String pass, String host, String dbName) {
        try {
            DiscordGuard.getInstance().getLogger().info("Connecting to database - " + dbName);
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://" + host + "/" + dbName + "?characterEncoding=UTF-8&serverTimezone=UTC", user, pass);
            DiscordGuard.getInstance().getLogger().info("Connect is ready!");
//            checkAndCreateTable();
        } catch (Exception ex) {
//            if (createDataBase(host, dbName, user, pass)) {
            try {
                DiscordGuard.getInstance().getLogger().info("Connecting to database - " + dbName + " ...Second attempt");
                Class.forName("com.mysql.cj.jdbc.Driver");
                conn = DriverManager.getConnection("jdbc:mysql://" + host + "/" + dbName + "?characterEncoding=UTF-8&serverTimezone=UTC", user, pass);
                DiscordGuard.getInstance().getLogger().info("Connect is ready!");
                //checkAndCreateTable();
            } catch (Exception ex2) {
                conn = null;
                ex2.printStackTrace();
            }
//            }
        }
        return conn;
    }

    public void closer(ResultSet rs, PreparedStatement ps) {
        if (rs != null) {
            closer(rs);
        }
        if (ps != null) {
            closer(ps);
        }
    }

    public void closer(ResultSet rs) {
        try {
            rs.close();
        } catch (Exception ex) {
        }
    }

    public void closer(PreparedStatement ps) {
        try {
            ps.close();
        } catch (Exception ex) {
        }
    }

}
