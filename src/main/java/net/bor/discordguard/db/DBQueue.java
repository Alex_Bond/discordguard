package net.bor.discordguard.db;

// @author ArtBorax
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class DBQueue {

    private final DataBases db;
    private final Queue<QueueData> queueQuery = new ConcurrentLinkedQueue<>();

    public DBQueue(DataBases db) {
        this.db = db;
        startManagerQueue();
    }

    private class QueueData {

        public final Object[] params;
        public String query;

        public QueueData(String query, Object... params) {
            this.params = params;
            this.query = query;
        }
    }

    public void addQueueQuery(String query, Object... params) {
        queueQuery.add(new QueueData(query, params));
    }

    private void startManagerQueue() {
        Thread t = new Thread() {
            @Override
            public void run() {
                while (true) {
                    PreparedStatement ps = null;
                    try {
                        while (!queueQuery.isEmpty()) {
                            QueueData queryData = queueQuery.remove();
                            ps = db.getConnectDBDiscordGuard().prepareStatement(queryData.query);
                            for (int i = 1; i <= queryData.params.length; i++) {
                                ps.setObject(i, queryData.params[i - 1]);
                            }
                            ps.executeUpdate();

//                            ps.executeUpdate(queryData.toString(), PreparedStatement.RETURN_GENERATED_KEYS); 
//                            ps.executeQuery(); 
                            ResultSet rs = ps.getGeneratedKeys();
                            if (rs.next()) {
                                System.out.println(rs.getInt(1));
                            }

                            db.closer(rs, ps);
                            Thread.sleep(100);
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    } finally {
                        db.closer(ps);
                    }
                }
            }
        };
        t.start();
    }

}
