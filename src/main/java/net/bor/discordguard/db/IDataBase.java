package net.bor.discordguard.db;

// @author ArtBorax
import java.util.ArrayList;

public interface IDataBase {

    public String getUsername(int id);

    public int getId(String username);

    public ArrayList<String> getServerUserHistory(int srvId, int userId, int size);

    public ArrayList<String> getUserHistory(int userId, int size);

    public ArrayList<String> getServerAllHistory(int srvId, int userId, int size);

    public ArrayList<String> getServerHistoryCens(int srvId, int userId, int size);

    public ArrayList<String> getHistoryCens(int userId, int size);

    public ArrayList<String> getDossier(int userId);

    public int addUserHistory(int userId, int srvId, String msg, int time);

    public int addUserHistoryCens(int IdMsg);

    public int addDossier(int userId, String msg);

    public int addDossier(int userId, int srvId, String msg);

    public int addDossier(int userId, String msg, int level, String punisher);

    public int addDossier(int userId, int srvId, String msg, int level, String punisher);

    public int getKarma(int userId);

    public void setKarma(int userId, int karma);

    public void updateJoinTime(int srvId, int userId, int datetime);

    public void updateQuitTime(int srvId, int userId, int datetime);

    public int getJoinTime(int srvId, int userId);

    public int getJoinCount(int srvId, int userId);

    public int getQuitTime(int srvId, int userId);

    public int getOnlineTime(int srvId, int userId);
    
    public String getPassHashUser(int userId);
    
    public String getUsername(long userDiscordId);
    
    public boolean setDossierLinkUser(int userId, long userDiscordId);
    
    public String getRegisterDate(int userId);
    
    public String getLastvisitDate(int userId);
    
    public boolean getBlockUser(int userId);
    
    public ArrayList<String> getBanUser(int userId);
}
