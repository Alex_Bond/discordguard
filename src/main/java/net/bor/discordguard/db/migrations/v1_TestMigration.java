package net.bor.discordguard.db.migrations;

import net.bor.discordguard.DiscordGuard;
import net.bor.discordguard.utilities.migrations.Migration;
import org.jooq.DSLContext;

public class v1_TestMigration extends Migration {

    public v1_TestMigration(DSLContext db) {
        super(db);
    }

    @Override
    public boolean up() {
        DiscordGuard.getInstance().getLogger().info("Test migration");
        return true;
    }

    @Override
    public boolean down() {
        return false;
    }
}
