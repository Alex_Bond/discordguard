package net.bor.discordguard;

// @author ArtBorax
public class Settings {

    public static final String CONFIG_FILENAME = "configs/config.yml";
    public static final String LOG_FILENAME = "server.log";

    public static final String LANG_UNKNOWN_CODE = "Я не знаю такого кода, попробуйте еще раз.";
    public static final String LANG_INVALID_CODE = "Ты уверен, что это твой код? Код состоит из 4 цифр.";
    public static final String LANG_DISCORD_ACCOUNT_LINKED = "Твоя учетная запись Discord связана с %name%";
}
