package net.bor.discordguard.discordbot;

import net.dv8tion.jda.core.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

// @author ArtBorax
public class DiscordAccountLinkListener extends ListenerAdapter {

    private final DiscordBot discordBot;

    public DiscordAccountLinkListener(DiscordBot discordBot) {
        this.discordBot = discordBot;
    }

    @Override
    public void onPrivateMessageReceived(PrivateMessageReceivedEvent event) {
        if (event.getAuthor().getIdLong() == event.getJDA().getSelfUser().getIdLong()) {
            return;
        }
    }

}
