package net.bor.discordguard.discordbot.event;

import net.dv8tion.jda.core.JDA;

// @author ArtBorax
abstract class DiscordEvent<T> extends Event {

    final private JDA jda;
    final private T rawEvent;

    DiscordEvent(JDA jda) {
        this.jda = jda;
        this.rawEvent = null;
    }

    DiscordEvent(JDA jda, T rawEvent) {
        this.jda = jda;
        this.rawEvent = rawEvent;
    }

    public JDA getJda() {
        return jda;
    }

    public T getRawEvent() {
        return rawEvent;
    }

}
