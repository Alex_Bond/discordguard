package net.bor.discordguard.discordbot.event;

import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.PrivateChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.priv.PrivateMessageReceivedEvent;

// @author ArtBorax
public class DiscordPrivateMessageReceivedEvent extends DiscordEvent<PrivateMessageReceivedEvent> {

    public User getAuthor() {
        return author;
    }

    public PrivateChannel getChannel() {
        return channel;
    }

    public Message getMessage() {
        return message;
    }

    private final User author;
    private final PrivateChannel channel;
    private final Message message;

    public DiscordPrivateMessageReceivedEvent(PrivateMessageReceivedEvent jdaEvent) {
        super(jdaEvent.getJDA(), jdaEvent);
        this.author = jdaEvent.getAuthor();
        this.channel = jdaEvent.getChannel();
        this.message = jdaEvent.getMessage();
    }
}
