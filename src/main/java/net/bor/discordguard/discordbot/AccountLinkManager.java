package net.bor.discordguard.discordbot;

import java.util.HashMap;
import java.util.Map;
import net.bor.discordguard.Settings;

// @author ArtBorax
public class AccountLinkManager {

    private final Map<String, Integer> linkingCodes = new HashMap<>();

    public String process(String linkCode, long discordUserId) {
        linkCode = linkCode.replaceAll("[^0-9]", "");

        if (linkingCodes.containsKey(linkCode)) {
            link(discordUserId, linkingCodes.get(linkCode));
            linkingCodes.remove(linkCode);

            //отправить сообщение на сервер
            return Settings.LANG_DISCORD_ACCOUNT_LINKED.replace("%name%", "получить имя игрока");
        }

        return linkCode.length() == 4 ? Settings.LANG_UNKNOWN_CODE : Settings.LANG_INVALID_CODE;
    }

    public void link(long discordUserId, int userId) {

    }
}
