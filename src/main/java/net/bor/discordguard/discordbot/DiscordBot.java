package net.bor.discordguard.discordbot;

// @author ArtBorax

import java.util.HashMap;
import javax.security.auth.login.LoginException;

import net.bor.discordguard.DiscordGuard;
import net.bor.discordguard.config.IConfig;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.TextChannel;

public class DiscordBot {

    private static boolean isReady = false;

    private JDA jda;
    private TextChannel consoleChannel;
    private TextChannel censChannel;
    private HashMap<Integer, TextChannel> chanels = new HashMap<>();

    private final IConfig config;

    public DiscordBot(IConfig config) {
        this.config = config;
    }

    public void init() {

        // выключить если был запущен ранее и это перезагрузка
        if (jda != null) {
            // удалить все листнеры
            jda.getRegisteredListeners().forEach(o -> jda.removeEventListener(o));
            try {
                jda.shutdown();
                jda = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // login to discord
        try {
            JDABuilder jdaBuild = new JDABuilder(AccountType.BOT)
                    .setAudioEnabled(false)
                    .setAutoReconnect(true)
                    .setBulkDeleteSplittingEnabled(false)
                    .setToken(config.getDiscordToken())
                    .addEventListener(new DiscordConsoleListener(this))
                    .addEventListener(new DiscordCensListener(this))
                    .addEventListener(new DiscordAccountLinkListener(this));

            for (int serverId : config.getServersIdChanelId().keySet()) {
                jdaBuild.addEventListener(new DiscordChatListener(this, serverId));
            }

            jda = jdaBuild.buildAsync();
        } catch (LoginException e) {
            DiscordGuard.getInstance().getLogger().error("LoginException: " + e.getMessage());
            return;
        } catch (Exception e) {
            DiscordGuard.getInstance().getLogger().error("Неизвестная ошибка при построении JDA...");
            e.printStackTrace();
            return;
        }

        while (jda.getStatus() != JDA.Status.CONNECTED) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
        }

        // game status
        if (!config.getDiscordGameStatus().isEmpty()) {
            DiscordUtil.setGameStatus(config.getDiscordGameStatus());
        }

        // распечатать, что видно боту
        for (Guild server : jda.getGuilds()) {
            DiscordGuard.getInstance().getLogger().trace("server: " + server);
            for (TextChannel channel : server.getTextChannels()) {
                DiscordGuard.getInstance().getLogger().trace("- " + channel);
            }
        }

        if (jda.getGuilds().size() == 0) {
            DiscordGuard.getInstance().getLogger().error("Нет не одного подключенного сервера Discord");
            return;
        }

        // set console channel
        if (config.getChanelIdConsole() != 0) {
            consoleChannel = DiscordUtil.getTextChannelById(config.getChanelIdConsole());
        }

        // set cens channel
        if (config.getChanelIdCens() != 0) {
            censChannel = DiscordUtil.getTextChannelById(config.getChanelIdCens());
        }

        chanels.clear();
        for (int serverId : config.getServersIdChanelId().keySet()) {
            chanels.put(serverId, DiscordUtil.getTextChannelById(config.getServersIdChanelId().get(serverId)));
        }

        // set ready status
        if (jda.getStatus() == JDA.Status.CONNECTED) {
            isReady = true;
        }
    }

    public JDA getJda() {
        return jda;
    }

    public TextChannel getConsoleChannel() {
        return consoleChannel;
    }

    public TextChannel getCensChannel() {
        return censChannel;
    }

    public HashMap<Integer, TextChannel> getChannels() {
        return chanels;
    }

    public TextChannel getChannel(int serverId) {
        return chanels.get(serverId);
    }

    public boolean isReady() {
        return isReady;
    }
}
