package net.bor.discordguard.discordbot;

// @author ArtBorax

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.bor.discordguard.DiscordGuard;
import net.bor.discordguard.config.IConfig;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Game.GameType;
import net.dv8tion.jda.core.entities.TextChannel;

@RequiredArgsConstructor
public class DiscordUtil {

    @NonNull
    private static IConfig config;
    @NonNull
    private static DiscordBot discordBot;

    public static JDA getJda() {
        return discordBot.getJda();
    }

    public static void setGameStatus(String gameStatus) {
        if (getJda() == null) {
            DiscordGuard.getInstance().getLogger().error("Attempted to set game status using null JDA");
            return;
        }
        if (gameStatus.isEmpty()) {
            return;
        }
        getJda().getPresence().setGame(Game.of(GameType.DEFAULT, gameStatus));
    }

    public static TextChannel getTextChannelById(Long channelId) {
        try {
            return getJda().getTextChannelById(channelId);
        } catch (Exception ignored) {
            return null;
        }
    }
}
