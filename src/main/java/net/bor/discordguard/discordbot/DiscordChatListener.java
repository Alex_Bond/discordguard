package net.bor.discordguard.discordbot;

// @author ArtBorax
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class DiscordChatListener extends ListenerAdapter {

    private final DiscordBot discordBot;
    private final int serverId;

    public DiscordChatListener(DiscordBot discordBot, int serverId) {
        this.discordBot = discordBot;
        this.serverId = serverId;
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        if (event.getAuthor() == null || event.getMember() == null || event.getAuthor().getId() == null || DiscordUtil.getJda() == null || DiscordUtil.getJda().getSelfUser() == null || DiscordUtil.getJda().getSelfUser().getId() == null || event.getAuthor().equals(DiscordUtil.getJda().getSelfUser())) {
            return;
        }
        if (discordBot.getChannel(serverId) == null || event.getChannel().getIdLong() != discordBot.getChannel(serverId).getIdLong()) {
            return;
        }
    }
}
