package net.bor.discordguard.utilities.migrations;

public interface IMigration {
    public boolean up();

    public boolean down();
}
