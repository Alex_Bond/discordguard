package net.bor.discordguard.utilities.migrations;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;

@RequiredArgsConstructor
public abstract class Migration implements IMigration, Comparable<Migration> {

    @NonNull
    private DSLContext db;

    public abstract boolean up();

    public abstract boolean down();

    public int compareTo(@NonNull Migration other) {
        return this.getClass().getSimpleName().compareTo(other.getClass().getSimpleName());
    }

}
