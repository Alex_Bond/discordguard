package net.bor.discordguard.utilities.migrations;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.bor.discordguard.DiscordGuard;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.reflections.Reflections;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

@RequiredArgsConstructor
public class Migrations {

    @NonNull
    private Connection connection;

    private DSLContext context;

    public void init() {
        this.context = DSL.using(connection, SQLDialect.MARIADB);
        try {
            initMigrationTable();
        } catch (SQLException e) {
            DiscordGuard.getInstance().getLogger().fatal("Error while migrations initialization", e);
            Runtime.getRuntime().halt(1);
        }
    }

    public void migrateUp() throws Exception {
        List<Class<? extends Migration>> migrations = new ArrayList<>(this.getMigrationClasses());
        migrations.sort(Collections.reverseOrder());

        HashMap<String, Long> applied = this.getAppliedMigrations();

        migrations = migrations.stream().filter(migration -> !applied.containsKey(migration.getSimpleName())).collect(Collectors.toList());

        for (Class c : migrations) {
            DiscordGuard.getInstance().getLogger().info("Applying migration ...... " + c.getSimpleName());

            Migration migration = (Migration) c.newInstance();
            if (!migration.up()) {
                throw new Exception("Error while migrating");
            }

            this.context
                    .insertInto(table("migrations"), field("version"), field("apply_time"))
                    .values(c.getSimpleName(), DSL.currentTimestamp())
                    .execute();

            DiscordGuard.getInstance().getLogger().info("Migration applied ...... " + c.getSimpleName());
        }
    }

    private void initMigrationTable() throws SQLException {
        PreparedStatement stmt = connection.prepareStatement("SHOW TABLES LIKE 'migrations'");
        if (!stmt.executeQuery().next()) {
            this.context
                    .createTableIfNotExists("migrations")
                    .column("version", SQLDataType.VARCHAR(256).nullable(false))
                    .column("apply_time", SQLDataType.TIMESTAMP.nullable(false))
                    .execute();

            this.context
                    .createUniqueIndex("migrations_version_unique")
                    .on("migrations", "version")
                    .execute();
        }
    }

    private Set<Class<? extends Migration>> getMigrationClasses() {
        Reflections reflections = new Reflections("net.bor.discordguard.db.migrations");
        return reflections.getSubTypesOf(Migration.class);
    }

    private HashMap<String, Long> getAppliedMigrations() {
        Result<Record2<Object, Object>> results = this.context
                .select(field("version"), field("apply_time"))
                .from("migrations")
                .orderBy(
                        field("apply_time").desc(),
                        field("apply_time").desc()
                ).fetch();
        HashMap<String, Long> out = new HashMap<>();
        for (Record r : results) {
            out.put(r.getValue("version", String.class), r.getValue("apply_time", Long.class));
        }

        return out;
    }
}
