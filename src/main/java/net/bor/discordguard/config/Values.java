package net.bor.discordguard.config;

// @author ArtBorax
import java.util.HashMap;

public class Values {

    public String DBDiscordGuardHost = "127.0.0.1:3306";
    public String DBDiscordGuardName = "nameDB";
    public String DBDiscordGuardUsername = "username";
    public String DBDiscordGuardPass = "pass";
    public String DBUsersHost = "127.0.0.1:3306";
    public String DBUsersName = "nameDB";
    public String DBUsersUsername = "username";
    public String DBUsersPass = "pass";
    public String DBLauncherHost = "127.0.0.1:3306";
    public String DBLauncherName = "nameDB";
    public String DBLauncherUsername = "username";
    public String DBLauncherPass = "pass";
    public String DiscordToken = "token";
    public long DiscordServerId = 1000;
    public HashMap<Integer, Long> ServersIdChanelId = new HashMap<>();
    public long ChanelIdCens = 1001;
    public long ChanelIdConsole = 1002;
    public HashMap<Long, String> RoleIdPrefix = new HashMap<>();
    public String OtherPrefix = "Гость";
    public HashMap<Long, Integer> RoleIdPermissionLvl = new HashMap<>();
    public String DiscordGameStatus = "MCBorax.ru";
    public String DiscordCommandPrefix = ".";
    public int KarmaMaxReport = 200;
    public int KarmaShiftReport = 10;
    public int KarmaPlayerBan = -1000;
    public int KarmaPlayerGuard = 200;
    public int KarmaPlayerReporter = 5;
    public int KarmaMaxAutoUp = 10;
    public int KarmaNewPlayer = 0;
    public int KarmaPercentSpeed = 10;
    public double KarmaSpeed1minute = 1.0;
    public double KarmaKeffLevel = 40.0;
    public double KarmaMultLevel = 2.0;
    public int KarmaPenaltyLevel = 50;
    public int KarmaRewardReport = 10;
    public int KarmaPenaltyReport = 30;

    public Values() {
        ServersIdChanelId.put(1, 10001L);
        ServersIdChanelId.put(2, 10002L);
        ServersIdChanelId.put(3, 10003L);

        RoleIdPrefix.put(101L, "Страж");
        RoleIdPrefix.put(102L, "Игрок");
        RoleIdPrefix.put(103L, "Owner");

        RoleIdPermissionLvl.put(101L, 1);
        RoleIdPermissionLvl.put(102L, 2);
        RoleIdPermissionLvl.put(103L, 3);
        RoleIdPermissionLvl.put(104L, 4);
    }
}
