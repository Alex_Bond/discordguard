package net.bor.discordguard.config;

// @author ArtBorax

import com.esotericsoftware.yamlbeans.YamlConfig;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import net.bor.discordguard.DiscordGuard;
import net.bor.discordguard.Settings;

public class Config implements IConfig {

    private Values values;

    public Config() {
        load();
    }

    private void save(Values values) {
        try {
            YamlConfig config = new YamlConfig();
            config.writeConfig.setWriteClassname(YamlConfig.WriteClassName.NEVER);
            config.writeConfig.setWriteRootElementTags(false);
            config.writeConfig.setWriteRootTags(false);
            config.writeConfig.setExplicitFirstDocument(true);
            config.writeConfig.setWriteDefaultValues(true);
            config.writeConfig.setEscapeUnicode(false);
            YamlWriter writer = new YamlWriter(new FileWriter(Settings.CONFIG_FILENAME), config);

            writer.write(values);
            writer.close();
            DiscordGuard.getInstance().getLogger().warn("Default config save");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void load() {
        try {
            DiscordGuard.getInstance().getLogger().info("Loading config...");
            YamlReader reader = new YamlReader(new FileReader(Settings.CONFIG_FILENAME));
            values = reader.read(Values.class);
            reader.close();
            DiscordGuard.getInstance().getLogger().info("Сonfig loaded");
        } catch (FileNotFoundException ex) {
            DiscordGuard.getInstance().getLogger().warn("Default config created...");
            values = new Values();
            save(values);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void reload() {
        load();
    }

    @Override
    public String getDBDiscordGuardHost() {
        return values.DBDiscordGuardHost;
    }

    @Override
    public String getDBDiscordGuardName() {
        return values.DBDiscordGuardName;
    }

    @Override
    public String getDBDiscordGuardUsername() {
        return values.DBDiscordGuardUsername;
    }

    @Override
    public String getDBDiscordGuardPass() {
        return values.DBDiscordGuardPass;
    }

    @Override
    public String getDBUsersHost() {
        return values.DBUsersHost;
    }

    @Override
    public String getDBUsersName() {
        return values.DBUsersName;
    }

    @Override
    public String getDBUsersUsername() {
        return values.DBUsersUsername;
    }

    @Override
    public String getDBUsersPass() {
        return values.DBUsersPass;
    }

    @Override
    public String getDBLauncherHost() {
        return values.DBLauncherHost;
    }

    @Override
    public String getDBLauncherName() {
        return values.DBLauncherName;
    }

    @Override
    public String getDBLauncherUsername() {
        return values.DBLauncherUsername;
    }

    @Override
    public String getDBLauncherPass() {
        return values.DBLauncherPass;
    }

    @Override
    public String getDiscordToken() {
        return values.DiscordToken;
    }

    @Override
    public long getDiscordServerId() {
        return values.DiscordServerId;
    }

    @Override
    public HashMap<Integer, Long> getServersIdChanelId() {
        return values.ServersIdChanelId;
    }

    @Override
    public long getChanelIdCens() {
        return values.ChanelIdCens;
    }

    @Override
    public long getChanelIdConsole() {
        return values.ChanelIdConsole;
    }

    @Override
    public HashMap<Long, String> getRoleIdPrefix() {
        return values.RoleIdPrefix;
    }

    @Override
    public String getOtherPrefix() {
        return values.OtherPrefix;
    }

    @Override
    public HashMap<Long, Integer> getRoleIdPermissionLvl() {
        return values.RoleIdPermissionLvl;
    }

    @Override
    public String getDiscordGameStatus() {
        return values.DiscordGameStatus;
    }

    @Override
    public String getDiscordCommandPrefix() {
        return values.DiscordCommandPrefix;
    }

    @Override
    public int getKarmaMaxReport() {
        return values.KarmaMaxReport;
    }

    @Override
    public int getKarmaShiftReport() {
        return values.KarmaShiftReport;
    }

    @Override
    public int getKarmaPlayerBan() {
        return values.KarmaPlayerBan;
    }

    @Override
    public int getKarmaPlayerGuard() {
        return values.KarmaPlayerGuard;
    }

    @Override
    public int getKarmaPlayerReporter() {
        return values.KarmaPlayerReporter;
    }

    @Override
    public int getKarmaMaxAutoUp() {
        return values.KarmaMaxAutoUp;
    }

    @Override
    public int getKarmaNewPlayer() {
        return values.KarmaNewPlayer;
    }

    @Override
    public int getKarmaPercentSpeed() {
        return values.KarmaPercentSpeed;
    }

    @Override
    public double getKarmaSpeed1minute() {
        return values.KarmaSpeed1minute;
    }

    @Override
    public double getKarmaKeffLevel() {
        return values.KarmaKeffLevel;
    }

    @Override
    public double getKarmaMultLevel() {
        return values.KarmaMultLevel;
    }

    @Override
    public int getKarmaPenaltyLevel() {
        return values.KarmaPenaltyLevel;
    }

    @Override
    public int getKarmaRewardReport() {
        return values.KarmaRewardReport;
    }

    @Override
    public int getKarmaPenaltyReport() {
        return values.KarmaPenaltyReport;
    }

}
