package net.bor.discordguard.config;

// @author ArtBorax
import java.util.HashMap;

public interface IConfig {

    public void reload();

    public String getDBDiscordGuardHost();

    public String getDBDiscordGuardName();

    public String getDBDiscordGuardUsername();

    public String getDBDiscordGuardPass();

    public String getDBUsersHost();

    public String getDBUsersName();

    public String getDBUsersUsername();

    public String getDBUsersPass();

    public String getDBLauncherHost();

    public String getDBLauncherName();

    public String getDBLauncherUsername();

    public String getDBLauncherPass();

    public String getDiscordToken();

    public long getDiscordServerId();

    public HashMap<Integer, Long> getServersIdChanelId();

    public long getChanelIdCens();

    public long getChanelIdConsole();

    public HashMap<Long, String> getRoleIdPrefix();

    public String getOtherPrefix();

    public HashMap<Long, Integer> getRoleIdPermissionLvl();

    public String getDiscordGameStatus();

    public String getDiscordCommandPrefix();

    public int getKarmaMaxReport();

    public int getKarmaShiftReport();

    public int getKarmaPlayerBan();

    public int getKarmaPlayerGuard();

    public int getKarmaPlayerReporter();

    public int getKarmaMaxAutoUp();

    public int getKarmaNewPlayer();

    public int getKarmaPercentSpeed();

    public double getKarmaSpeed1minute();

    public double getKarmaKeffLevel();

    public double getKarmaMultLevel();

    public int getKarmaPenaltyLevel();

    public int getKarmaRewardReport();

    public int getKarmaPenaltyReport();

}
