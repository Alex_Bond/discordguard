#!/bin/bash

CMD="java"
if [ -n "$DEBUG" ]
then
  if [ "$DEBUG" -ge 1 ]
  then
    if [ -n "$DEBUG_WAIT" ]
    then
        if [ "$DEBUG_WAIT" -ge 1 ]
        then
            CMD="$CMD -agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=5005"
        else
            CMD="$CMD -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005"
        fi
    else
        CMD="$CMD -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005"
    fi
  fi
fi
CMD="$CMD -jar DiscordGuard.jar $JAVA_ARGS"
eval "$CMD"