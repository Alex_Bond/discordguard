Discord Guard Service
=====================

Сервис дискорд гарда. Описание еще надо придумать. Пока и так сойдет.

## Docker конфигурация

Используемые версии:

* Docker: 17.12.0-ce
* docker-compose: 1.18.0

### Старт системы

1. `docker-compose up` (`-d` опционально если не хотите оставлять логи в консольке)
2. Собрать образ из Dockerfile и запустить с подключением к сети или раскоментировать discord_guard сервис в композе и перезапустить композ

Пример билда и запуска:
```
docker build .
&& docker run
-p 127.0.0.1:5005:5005
-v /Users/alex/IdeaProjects/mcborax/discordguard/docker-mounts/configs:/java/configs
-v /Users/alex/IdeaProjects/mcborax/discordguard/docker-mounts/logs:/java/logs
-v /Users/alex/IdeaProjects/mcborax/discordguard/build/libs/DiscordGuard.jar:/java/DiscordGuard.jar
--env JAVA_ARGS=--debug
--env DEBUG=1
--name DiscordGuard
--network discordguard_serviceNetwork
```

## Конфигурация окружения

### Базы данных

Базы данных открыты наружу так:

* Discord Database: `127.0.0.1:3306`
* Users Database: `127.0.0.1:3307`
* Launcher Database: `127.0.0.1:3308`

Для подключения к базам внутри докера использовать такие параметры:

* Discord Database: `root:password@DiscordDB:3306`
* Users Database: `root:password@UsersDB:3306`
* Launcher Database: `root:password@LauncherDB:3306`

### Подключение к RabbitMQ

Сервер: `RabbitMQ`
Открытый порт для менеджмента: `127.0.0.1:15672`

Note: Кролику установлено ограничение в 1GB оперативки

## Параметры запуска

Для запуска в режиме дебага в IDE (JRE DEBUG MODE) указать переменную окружения `DEBUG` с значением `1`
Если хотите чтобы JRE ждала подключение IDE после запуска то укажите `DEBUG_WAIT` с значением `1`

Для передачи любых параметров при запуске добавляйте их в переменную окружения `JAVA_ARGS`.

### Доступные не стандартные параметры

| Параметр        | Назначения                            |
|-----------------|---------------------------------------|
| --debug         | Включение режима отладки сервиса      |
| --no-migrations | Пропуск применения миграций к системе |