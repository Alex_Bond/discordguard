FROM openjdk:8

RUN cd / && mkdir java
WORKDIR /java

EXPOSE 5005

COPY build/libs/DiscordGuard.jar /java
COPY docker/docker-init/run.sh /java
RUN chmod 0777 /java/run.sh

ENTRYPOINT ["/java/run.sh"]